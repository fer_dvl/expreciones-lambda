/*
	Interfaz Funcional
	Expresion Lambda con parametros
*/

public interface Operaciones{
	public void imprimeOperaciones(int num1, int num2);
}